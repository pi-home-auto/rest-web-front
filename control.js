//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner
//
//   control.js  Interface with the control service

var net = require('net');

var controlVersion = '0.1.3';
var keeprunning = true;
var client;
var userCallbacks = [];
var greeting;
var controlPort = 1800;
var isConnected = false;
var spns;
var strBuffer = "";

//TODO: Try to always keep connected to the control service.  If disconnected then
//      try to reconnect (in a loop).
exports.initialize = function(config) {
    console.log('Establish connection with control service at '
                + config.controlUri + ':' + config.controlPort);
    controlPort = config.controlPort;
    greeting =
	{ "greeting": {
	    "version": controlVersion,
	    "name": config.name,
	    "role": config.role,
	    "mainGroup": config.mainGroup,
	    "type": "rest" }
	};
    setImmediate(doConnect);
    setInterval(doConnect, 15000);
}

function doConnect() {
    if (isConnected) {
	//console.log('Already connected to controlservice.');
	return;
    }

    client = net.connect({ port: controlPort }, function() {
	isConnected = true;
        console.log('HomeautoServer connected to control service');
    });
    client.on('error', function(err) {
	isConnected = false;
	console.log('Got an error event in net: ' + err);
    });
    client.on('close', function() {
	isConnected = false;
	console.log('Socket closed to controlservice');
    });

    if (client) {
	client.write(JSON.stringify(greeting) + '\r\n\r\n');
	client.once('data', function(data) {
	    console.log('verify greeting from controlservice');

	    var jdata;
	    try {
		jdata = JSON.parse(data);
	    } catch (err) {
		console.log(err + ' payload: ' + data);
	    }
	    if (jdata && jdata.greeting) {
		console.log('Connected to controlservice: ' + jdata.greeting.name +
			    ', role=' + jdata.greeting.role +
                            ', version=' + jdata.greeting.version +
			    ', type=' + jdata.greeting.type);
		//TODO verify type is 'control'
                if (jdata.greeting.version != controlVersion) {
                    console.log('Version mismatch running ' + controlVersion +
                                ' peer running ' + jdata.greeting.version);
                }

		// Install global event handler on data read from socket
		client.on('data', readDataHandler);
	    } else {
		console.log('Error: invalid greeting from controlservice');
		client.end();
	    }
	});
	client.on('end', function() {
	    console.log('HomeautoServer disconnected from controlservice');
	});
    } else {
	console.log('Trying to connect to controlservice ...');
    }
}

exports.send = function(json, onDataCb) {
    if (onDataCb !== undefined) {
	userCallbacks.push(onDataCb);
    }

    if (!(json.control || json.statistics)) {	// need to wrap in a control header
	console.log('Error: missing control header in message');
    }
    console.log(' SEND to control: ' + JSON.stringify(json));
    client.write(JSON.stringify(json) + '\r\n\r\n');
}

exports.setNotification = function(spnsObj) {
    console.log("setting notification object for control service");
    spns = spnsObj;
}

exports.wrapCommand = function(json) {
    var wrapped = { "control": json };
    return wrapped;
}

/**
 * This is the on.data event handler for the socket connection.
 * If a user defined callback is set then it is called.
 * Otherwise, this method will handle the IPC messages.  Some messages
 * such as shutdown and goodbye are handled here before dispatch
 */
function readDataHandler(data) {
    console.log('control.readDataHandler got data: ' + data);
    var strdata = data.toString();
    strBuffer += strdata;
    //console.log('last 4=\"' + strdata.substring(strdata.length - 4) + '\"');
    if (strdata.substring(strdata.length - 4) != '\r\n\r\n') {
        console.log('waiting for end of buffer');
        return;  // buffer incomplete -- wait for more
    }

    var jmsgarr = strBuffer.split('\r\n\r\n');
    strBuffer = "";
    for (var i in jmsgarr) {
	if (jmsgarr[i].length < 10) return;

	//console.log('data: ++' + jmsgarr[i] + '--');
        var jdata;
        try {
            jdata = JSON.parse(jmsgarr[i]);
        } catch (err) {
            console.log(err);
        }

        if (!jdata) {
	    console.log('Bad data from controlservice: ' + strBuffer);
	    if (userCallbacks.length > 0) {
		var userCallback = userCallbacks.shift();
	        var reply =
		    { "reply": {
		        "status": "error",
		        "message": "cannot parse response from control service",
		        "data": data
		    } };
	        userCallback(reply);
	    }
	} else if (jdata.shutdown || jdata.goodbye) {
	    console.log('controlservice is exiting...closing connection.');
	    if (jdata.shutdown) keeprunning = false;
	    client.end();
	    return;
        } else if (jdata.controlalert) {
            var ctrlalert = jdata.controlalert;
	    console.log('got control alert event: ' + JSON.stringify(ctrlalert));
            if (ctrlalert.param1) {
                if (ctrlalert.method == 'fileEvent') {
                    var fileEvent = {
                        "device": "filemonitor",
                        "event": ctrlalert.statustext,
                        "file": ctrlalert.param1,
                        "contents": ctrlalert.param2
                    };
                    if (spns) {
                        spns.send(JSON.stringify(fileEvent));
                    } else {
                        console.log('Error: spns not set for control service');
                    }
                } else if (ctrlalert.method == 'key') {
                    var userId = ctrlalert.param2.replace('\n', '');
                    var garageEvent = {
                        "device": "garagecontrol",
                        "deviceType": "remote",
                        "user": userId
                    };
                    if (spns) {
                        spns.send(JSON.stringify(garageEvent));
                    } else {
                        console.log('Error: spns not set for control service');
                    }
                }
            }
        }
	else if (userCallbacks.length > 0) {
	    var userCallback = userCallbacks.shift();
	    console.log('call user callback');
	    userCallback(jdata);
	}
    }
}
