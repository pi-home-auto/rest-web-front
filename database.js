//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner
//
//   database.js  Interface to the SQL database

const fs = require("fs");
const os = require("os");
const sqlite3 = require("sqlite3").verbose();

var DBFILE = process.env.HA_BASE_DIR + "/data/" + process.env.HA_DBFILE;
var MEDIALIB = process.env.HA_BASE_DIR + "/incoming/media/";

var dbSelectStmt;
var dbSelectTypeStmt;
var dbGetDeviceStmt;
var dbListMediaStmt;

var deviceTypes = [];
var handlers = [];
var mainGroup;
var myConfig;
var moduleConfig = [];

// Old REST: <baseurl>/GPIO/<gp#>/sequence/<delay>,<bits>
// New REST: use a command table, so it can use names like On, Off, Up, Down,
//     Reset, Read, Toggle, Normal, etc.  These correspond with the "command" table.
// Newer REST: uses handlers that are attached to device_type
// TODO: add voice commands/handler.  configuration, component/unit

exports.open = function() {
    var exists = fs.existsSync(DBFILE);
    if (!exists) {
        console.log(DBFILE + ' is not initialized.  Please run initdb.sh and restart.');
        process.exit(1);
    } else {
	console.log('Opening database file ' + DBFILE);
    }
    var db = new sqlite3.Database(DBFILE);
    return db;
}

exports.initialize = function(db, callback) {
    readConfig(db, function() {
        console.log('after readConfig, mainGroup is ' + mainGroup);

        dbSelectStmt = db.prepare(
	    "SELECT uuid,name,location,type,addresses,parameters,purpose,module_name FROM device " +
	        "WHERE grouping = '" + mainGroup + "' AND state = 'enabled'");
        dbSelectPurposeStmt = db.prepare(
	    "SELECT uuid,name,location,type,addresses,parameters,purpose,module_name FROM device " +
	        "WHERE grouping = '" + mainGroup + "' AND state = 'enabled' AND purpose = ?");
        dbSelectTypeStmt = db.prepare(
	    "SELECT uuid,name,location,type,addresses,parameters,purpose,module_name FROM device " +
	        "WHERE grouping = '" + mainGroup + "' AND state = 'enabled' AND type = ?");
        dbGetDeviceStmt = db.prepare(
	    "SELECT uuid,name,location,type,addresses,parameters,module_name FROM device " +
	        "WHERE uuid = ?");
        dbListMediaStmt = db.prepare(
            "SELECT filename, description, mediatype, timestamp FROM media_file " +
		"WHERE mediatype = ? ORDER BY timestamp DESC");
        dbListTwoMediaStmt = db.prepare(
            "SELECT filename, description, mediatype, timestamp FROM media_file " +
		"WHERE mediatype = ? OR mediatype = ? ORDER BY mediatype, timestamp DESC");
        callback(myConfig);
    });

    readHandlers(db);	// this reads deviceTypes too
}

/* TODO: take a table name and list of columns as input and return a json array */
exports.sqlToJson = function(db, table, columns, criteria) {
    var sqls = "SELECT " + columns + " FROM " + table;
    if (criteria) sqls += " " + criteria;

    var stmt = db.prepare(sqls);
    var json = [];
    stmt.each(function(err, row) {
	console.log('Got a row, type = ' + typeof(row));
    });
    return json;
}

function readConfig(db, callback) {
    var selstr = "SELECT c.module_name, c.log_level, c.main_group, " +
                        "sm.url, sm.control_uri, sm.control_port " +
                         "FROM configuration AS c, system_module AS sm " +
                         "WHERE c.module_name = sm.name";

    // First, get my own configuration
    //var hostname = os.hostname();
    // We are spoofing hostname for testing
    var hostname = 'switchback';
    console.log('Read MY configuration for hostname: ' + hostname);
    var stmt = db.prepare(selstr + " AND c.module_name = \"" + hostname + "\"");
    stmt.get(function(err, row) {
        if (row != undefined) {
	    mainGroup = row.main_group;
	    myConfig = { "name": row.module_name,
                         "role": "base",
                         "logLevel":    row.log_level,
                         "controlUri":  row.control_uri,
                         "controlPort": row.control_port,
                         "mainGroup":   row.main_group
                       };
	    console.log('Configuration loaded for ' + myConfig.role + ', group ' + mainGroup);
            callback();
        }
    });

    stmt = db.prepare(selstr + " AND c.module_name != \"" + hostname + "\"");
    console.log('Read in configuration for other modules');
    moduleConfig = [];
    stmt.each(function(err, row) {
        if (row != undefined) {
            moduleConfig.push(
                { "name": row.name,
                  "role": "base",
                  "mainGroup": row.main_group,
                  "moduleUrl": row.url
                });
        } else {
            console.log('Error reading module configuration: ' + err);
        }
    }, function() {
        console.log(moduleConfig.length + " modules loaded");
    });
}

function readDeviceTypes(db) {
    var stmt = db.prepare("SELECT name,handler,handler2 FROM device_type " +
			  "ORDER BY name");
    console.log('Read in device_type table');
    deviceTypes = [];
    var num = 0;
    stmt.each(function (err, row) {
        var handler = getHandler(row.handler);
        var devrec = { name: row.name,
                       handler:  handler,
                       handlerName:  row.handler,
                     };
        if (row.handler2) {
            var handler2 = getHandler(row.handler2);
            devrec.handler2 = handler2;
            devrec.handler2Name = row.handler2;
        }
        deviceTypes[row.name] = devrec;
        ++num;
    }, function () {
	console.log(num + " device types loaded");
    });
}

function readHandlers(db) {
    var stmt = db.prepare("SELECT name,classname,parameters,preload FROM handler " +
	                  "ORDER BY name");
    console.log('Initialize handler table');
    handlers = [];
    stmt.each(function (err, row) {
        if (row.preload) {
            var hdlr = require("./handlers/" + row.name);
            var caps = hdlr.query();
            if (caps.capabilities.init) {
                caps.capabilities.init();
            }
            handlers.push(
                { name: row.name,
                  classname: row.classname,
                  parameters: "" + row.parameters,
                  caps: caps
                });
        }
    }, function () {
        console.log(handlers.length + " handlers loaded");
        readDeviceTypes(db);	// now link handlers into deviceType array
    });
}

exports.getConfig = function() {
    return myConfig;
}

exports.getDeviceType = function(name, callback) {
    if (deviceTypes[name]) {
        const devType = deviceTypes[name];
        callback(devType);
        return devType;
    }
    console.log("Device type " + name + " not found");
}

function getHandler(name) {
    for (var idx in handlers) {
        var handler = handlers[idx];
        if (handler.name == name)
            return handler;
    }
    console.log("Handler " + name + " not found");
}
exports.getHandler = getHandler;

function getModule(name) {
    for (var idx in moduleConfig) {
        var modConf = moduleConfig[idx];
        if (modConf.name == name)
            return modConf;
    }
    console.log("Module " + name + " not found");
}
exports.getModule = getModule;

exports.readDevice = function(db, device, handler) {
    "use strict";
    var jsonData = { device: { uuid: device, error: "Not found" } };
    console.log('doing readDevice(' + device + ')');
    dbGetDeviceStmt.get(device, function(err, row) {
        if (row != undefined) {
            jsonData =
		{ device: { uuid: row.uuid,
			    name: row.name,
			    location: row.location,
			    type: row.type,
			    group: mainGroup,
			    addresses: row.addresses,
			    parameters: row.parameters,
			    "module": row.module_name }
		};
            console.log('readDev:: ' + JSON.stringify(jsonData));
        }
        handler(jsonData);
    });
}

exports.getDevice = function(db, device, response) {
    "use strict";
    var jsonData = { device: { uuid: device, error: "Not found" } };
    console.log('doing getDevice(' + device + ')');
    dbGetDeviceStmt.get(device, function(err, row) {
        if (row != undefined) {
            jsonData =
		{ device: { uuid: row.uuid,
			    name: row.name,
			    type: row.type,
			    group: mainGroup,
			    addresses: row.addresses,
                            parameters: row.parameters }
		};
        }
        response.send(jsonData);
    });
}

exports.getDevicesList = function(db, callback, devtype, purpose) {
    "use strict";
    var jsonData = { devices: [] };
    var prepStmt;
    console.log('doing getDevicesList for group ' + mainGroup);

    if (purpose) {
        console.log('purpose = ' + purpose);
        prepStmt = dbSelectPurposeStmt.bind(purpose);
    } else if (devtype) {
        console.log('devtype = ' + devtype);
        prepStmt = dbSelectTypeStmt.bind(devtype);
    } else {
        console.log('select all devices');
        //prepStmt = dbSelectStmt.reset();
        prepStmt = dbSelectStmt;
    }
    prepStmt.each(function (err, row) {
        if (err) {
            console.log('Got error: ' + err + ' doing prepStmt.each()');
        } else {
            console.log('Got device ' + row.uuid);
            jsonData.devices.push(
	        { device: { uuid: row.uuid,
	                    name: row.name,
	                    location: row.location,
	                    type: row.type,
	                    group: mainGroup,
	                    addresses: row.addresses,
                            parameters: row.parameters,
                            purpose: row.purpose,
                            module: row.module_name,
                            setting: 0
                          }
	        });
	}
    }, function () {
        callback(jsonData);
    });
}

exports.getMediaJson = function(db, type, response, callback) {
    jsonData = { mediafiles: [] };
    if (type == 'picture') {
        dbListTwoMediaStmt.each('image', 'video', function (err, row) {
            jsonData.mediafiles.push(
	        { mediatype: row.mediatype,
	          filename: row.filename,
	          description: row.description,
	          timestamp: row.timestamp
	        });
        }, function () {
            callback(response, jsonData);
        });
    } else {
        dbListMediaStmt.each(type, function (err, row) {
            jsonData.mediafiles.push(
	        { mediatype: type,
	          filename: row.filename,
	          description: row.description,
	          timestamp: row.timestamp
	        });
        }, function () {
            callback(response, jsonData);
        });
    }
}

exports.deleteMediaFile = function(db, type, vidfile, response) {
    var stmt = db.prepare("DELETE FROM media_file WHERE filename = ? AND mediatype = ?");
    stmt.run(vidfile, type);
    var fpath = MEDIALIB + type + "s/" + vidfile;
    fs.unlink(fpath, function(err) {
        if (err) {
            console.log('Unable to delete ' + fpath);
        } else {
            console.log('Deleted ' + fpath);
        }
    });
    sendJsonStatus(response, 200, "Deleted", "OK");
}

exports.putMediaFile = function(db, type, vidfile, response) {
    var stmt = db.prepare("INSERT OR REPLACE INTO media_file(filename, mediatype, source_uuid, timestamp) VALUES (?,?,?,?)");
    stmt.run(vidfile, type, "CAM-123-1", new Date().toJSON());
    if (response) sendJsonStatus(response, 200, "Created " + vidfile, "OK");
}

function sendJsonStatus(response, code, message, state) {
    // where state is: [ "fatal" | "error" | "retry" | "warning" | "OK" ]
    console.log('Json status: ' + code + ' ' + message);
    var jsonData = {
	"status": {
            "code":  code,
            "message":  message,
            "state":  state
	}
    };
    if (response !== null) {
	response.send(jsonData);
    }
    return jsonData;
}
exports.sendJsonStatus = sendJsonStatus;
