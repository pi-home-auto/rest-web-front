//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

// Gpio.js -- GPIO handler.  Call controlservice to set and query a gpio pin

var control = require('../control');

exports.query = function() {
    var caps = {
	"name": "Gpio",
	"class": "",
	"capabilities": {
	    "init":  init,
	    "read":  read,
	    "write": write
	}
    };

    return caps;
}

function init() {
    console.log('Initialize Gpio handler in node.js');
}

function read(device, na, na, callback) {
    var retval;
    var addr = device.device.addresses.split(" ")[0];
    var pin = Number(addr);
    var json = { "handler": "gpio",
		 "method": "readkey",
                 "module": device.device.module,
                 "uuid": device.device.uuid,
		 "pin": pin,
		 "param1": device.device.addresses };
    var ctrljson = control.wrapCommand(json);
    console.log('Gpio.read send: ' + JSON.stringify(ctrljson));
    control.send(ctrljson, function(jdata) {
        console.log('Gpio callback got message');
        if (device.device.response) {
            console.log('Gpio sending response');
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);

        } else {
            console.log('No response set for Gpio from read method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
    });

    return retval;
}

function write(device, value, program, callback) {
    var retval;
    var json;
    if (value === 'program' && program !== null) {
        json = { "handler": "gpio",
		 "method": "program",
		 "param1": "start",
	         "param2": program };
    } else {
	var addr = device.device.addresses.split(" ")[0];
	var pin = Number(addr);
        json = { "handler": "gpio",
		 "method": "writekey",
                 "module": device.device.module,
                 "uuid": device.device.uuid,
		 "pin": pin,
		 "param1": device.device.addresses,
	         "param2": Number(value) };
    }
    var ctrljson = control.wrapCommand(json);
    console.log('Gpio.write send: ' + JSON.stringify(ctrljson));
    control.send(ctrljson, function(jdata) {
        console.log('Gpio callback got message');
        if (device.device.response) {
            console.log('Gpio sending response');
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
            else if (jdata.reply.param2) {
                device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);

        } else {
            console.log('No response set for Gpio from write method');
        }
        retval = jdata;
        if (callback != undefined) {
            callback(jdata);
        }
    });
    
    return retval;
}
