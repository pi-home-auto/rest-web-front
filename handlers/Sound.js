//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

var control = require('../control');

exports.query = function() {
var caps = {
    "name": "Sound",
    "class": "",
    "capabilities": {
        "init":  init,
        "open":  open,
        "close": close
        }
    };

    return caps;
};

function init() {
    console.log('Initialize Sound handler');
}

function open(device, filename, na, callback) {
    console.log('In sound Open');
    var json = { "handler": "sound",
                 "method": "play",
                 "param1": device.device.addresses,
                 "filename": filename
               };
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
        if (device.device.response) {
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
	        device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);
        } else {
            console.log('No response set for Sound from open method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
        return retval;
    });
}

function close(device, na, na, callback) {
    console.log('In sound Close');
    var json = { "handler": "sound",
                 "method": "stop",
                 "param1": device.device.addresses
               };
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
        if (device.device.response) {
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
	        device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);
        } else {
            console.log('No response set for Sound from close method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
        return retval;
    });
}
