//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

var control = require('../control');

exports.query = function() {
var caps = {
    "name": "Servo",
    "class": "",
    "capabilities": {
        "init":  init,
        "read":  read,
        "write": write
        }
    };

    return caps;
};

function init() {
    console.log('Initialize Servo handler');
}

function read(device, na, na, callback) {
    console.log('In servo Read');
    var json = { "handler": "servo",
                 "method": "readkey",
                 "param1": device.device.addresses
               };
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
        if (device.device.response) {
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
	        device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);
        } else {
            console.log('No response set for Servo from read method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
        return retval;
    });
}

function write(device,value,na,callback) {
    console.log('In servo Write: ' + JSON.stringify(value));
    valueJson = value;
    var json = {handler:"servo"};
    if (valueJson.action === "moveTo") {
        json.method = "moveTo";
        json.degrees = parseInt(valueJson.degrees);
        json.servo = parseInt(valueJson.servo);
    }
    else if (valueJson.action === "move") {
        json.method = "move";
        json.degrees = parseFloat(valueJson.degrees);
        json.servo = parseInt(valueJson.servo);
    }
    else {
        device.device.response.send('Unrecognized message');
        return;
    }
    console.log("The msg to control service is " + json);
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
            if(jdata === undefined) {
                console.log('No data from control service');
            }
            else {
                console.log('got ' + jdata + "from cs");
            }
            data = {
            "status": {
                    "code": 200,
                    "message": 'Servo Message received by controlservice',
                    "state":   'ok'
            }
            };
            device.device.response.send(data);
    });
}
