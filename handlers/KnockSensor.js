//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

// KnockSensor.js

var request = require("request");
var spns;

exports.query = function() {
    var caps = {
	"name": "KnockSensor",
	"class": "",
	"capabilities": {
	    "init":  init,
	    "write": write,
        "read" : read,
        "ioctl" : ioctl
	}
    };

    return caps;
};

function init() {

    console.log('Initialize KnockSensor');

}

function ioctl(value) {
    // this ioctl is for setting the spns variable.
    console.log("setting spns object in knocksensor");
    spns = value;
}


function read(device,na,na,callback) {
    console.log('The Knock Sensor read');
    request.get("http://"+device.device.addresses+":5000/knockinfo",function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("KnockStats will be sent to phone");
        }
        if(error) {
            console.log("error occurred" + error + " could not get KnockSensor");
            doorState = error;
        }
    });
    device.device.response.send('OK');
}


function write(device, value,na,callback) {
    console.log(' Address is ' + device.device.addresses + ' value :' + value );
    if (!device.device.addresses) {
        device.device.response.send('error');
        return;
    }
    var values = value.split(":");
    if (values.length < 2){
        device.device.response.send('error');
        return;
    }
    if(!spns){
        device.device.response.send('SPNS not initilized');
        return;
    }

    switch(values[0]) {
        case "RecordKnock":
            var postData = {knockId:values[1]};
            var urlToPost = "http://" + device.device.addresses + ":5000/recordknock";

            request.post(urlToPost,{ form: postData },function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        console.log(body);
                        var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"recordPattern",userId:postData.knockId};
                        spns.send(JSON.stringify(spnsData));
                    }
                    if(error) {
                        console.log("error occurred" + error);
                        var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"doorNotReachable"};
                        spns.send(JSON.stringify(spnsData));
                    }
                }
            );
            break;
        case "ConfirmPending":
            if (values.length < 3)
                return "error";
            var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"confirmPending",userId:values[1],numKnocks:values[2]};
            spns.send(JSON.stringify(spnsData));
            //spns.send("A knock Pattern with "+values[2]+" knocks is recorded for user:"+ values[1]+
            //    ", record again to confirm");
            break;
        case "KnockPatternConfirmed":
            if (values.length < 3)
                return "error";
            var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"knockPatternConfirmed",userId:values[1],numKnocks:values[2]};
            spns.send(JSON.stringify(spnsData));
            //spns.send("A knock Pattern with "+values[2]+" knocks is confirmed for user:"+ values[1]+
            //    ", record again to confirm");
            break;

        case "KnockPatternNotMatchedTryAgain":
            var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"knockPatternNotConfirmed",userId:values[1],numKnocks:values[2]};
            spns.send(JSON.stringify(spnsData));
            //spns.send("A knock Pattern for user:"+ values[1]+" is not confirmed, try recording again");
            break;
        case "KnockPatternDetected":
            var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"knockDetected",userId:values[1]};
            spns.send(JSON.stringify(spnsData));
            //spns.send("A knock pattern was detected for user " + values[1]);
            break;
        case "KnockPatternNotDetected":
             var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"knockNotDetected",numKnocks:values[1]};
            spns.send(JSON.stringify(spnsData));
            //spns.send("An invalid knock pattern was tried on door " + device.device.uuid);
            break;
        case "PatternExists":
             var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"PatternExists"};
            spns.send(JSON.stringify(spnsData));
            //spns.send("An invalid knock pattern was tried on door " + device.device.uuid);
            break;
        case "NotEnoughKnocks":
             var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"NotEnoughKnocks"};
            spns.send(JSON.stringify(spnsData));
            //spns.send("An invalid knock pattern wars tried on door " + device.device.uuid);
            break;
        case "KnockInfo":
            var knockInfo = "";
            for(var i=1;i<values.length;i++) {
                if (i===1)
                    knockInfo = values[i];
                else
                    knockInfo = knockInfo + ":"+values[i];
            }
            var knockInfoAry = JSON.parse(knockInfo);
            var spnsData = {device:device.device.uuid,deviceType:device.device.type,command:"KnockInfo",info:knockInfoAry};
            spns.send(JSON.stringify(spnsData));
    }



    device.device.response.send("OK");
}


