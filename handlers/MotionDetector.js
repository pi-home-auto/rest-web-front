//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

// MotionDetector.js -- Motion detector handler.
// Calls controlservice to query the state of the motion detector via a gpio pin.

var control = require('../control');

exports.query = function() {
    var caps = {
	"name": "MotionDetector",
	"class": "",
	"capabilities": {
	    "init":  init,
	    "read":  read
	}
    };

    return caps;
}

function init() {
    console.log('Initialize Gpio handler in node.js');
}

function read(device, na, na, callback) {
    var retval;
    var addr = device.device.addresses.split(" ")[0];
    var pin = Number(addr);
    var json = { "handler": "gpio",
		 "method": "readkey",
		 "pin": pin,
		 "param1": device.device.addresses };
    var ctrljson = control.wrapCommand(json);
    console.log('Gpio.read send: ' + JSON.stringify(ctrljson));
    control.send(ctrljson, function(jdata) {
        console.log('MotionDetector callback got message');
        if (device.device.response) {
            console.log('MotionDetector sending response');
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);

        } else {
            console.log('No response set for MotionDetector from read method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
    });

    return retval;
}
