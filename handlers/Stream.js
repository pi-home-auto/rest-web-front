//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

exports.query = function() {
    var caps = {
	"name": "Stream",
	"class": "",
	"capabilities": {
	    "read":  read,
	    "write": write,
	    "ioctl": ioctl
	}
    };

    return caps;
}

function read(device) {
    if (device.device.vidfmt) {
        var idx = (device.device.vidfmt == 'h264') ? 1 : 0;
        var params = device.device.parameters.split(",");
	//var host = request.headers.host.split(":");
	//var ret = params[idx].replace("@HOST@", host[0]);
	var ret = params[idx];
        console.log(' stream read returning: ' + ret);
        return ret;
    } else {
        console.log('ERROR: No video format set for camera device ' + device.device.uuid +
                    '\nUse ioctl to set vidfmt.');
    }
}

function write(device, value) {
}

function ioctl(device, nameValue) {
    //TODO: put valid ioctl constants in handler "caps" object and check it is valid
    //      before adding it to the device object.
    var name = nameValue[0];
    var value = nameValue[1];
    device.device[name] = value;
}
