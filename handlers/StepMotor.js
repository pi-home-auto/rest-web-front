//
//   Copyright 2021-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

var control = require('../control');

exports.query = function() {
var caps = {
    "name": "StepMotor",
    "class": "",
    "capabilities": {
        "init":  init,
        "read":  read,
        "write": write
        }
    };

    return caps;
};

function init() {
    console.log('Initialize StepMotor');
}

function read(device, na, na, callback) {
    console.log('In stepmotor Read: ' + value);
    var json = { "handler": "stepmotor",
                 "method": "readkey",
                 "module": device.device.module,
                 "uuid": device.device.uuid,
                 "param1": device.device.addresses
               };
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
        if (device.device.response) {
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
                    }
                };
            }
	    else if (jdata.reply.param2) {
	        device.device.setting = jdata.reply.param2;
            }
            device.device.response.send(jdata);
        } else {
            console.log('No response set for StepMotor from read method');
            if (jdata.reply.param2) {
		device.device.setting = jdata.reply.param2;
            }
        }
        retval = jdata;
	if (callback != undefined) {
	    callback(jdata);
	}
        return retval;
    });
}

function write(device,value,na,callback) {
    console.log('In stepmotor Write: ' + JSON.stringify(value));
    console.log('Modulename=' + device.device.module)
    valueJson = value;
    var json = { handler:"stepmotor",
                 module: device.device.module,
                 uuid: device.device.uuid };
    if (valueJson.action === "moveTo") {
        json.method = "moveTo";
        json.position = valueJson.position;
        json.speed = parseInt(valueJson.speed);
    }
    else if (valueJson.action === "move") {
        json.method = "move";
        json.steps = parseInt(valueJson.steps);
        json.speed = parseInt(valueJson.speed);
    }
    else {
        device.device.response.send('Unrecognized message');
        return;
    }
    console.log("The msg to control service is " + JSON.stringify(json));
    var ctrljson = control.wrapCommand(json);
    control.send(ctrljson, function(jdata) {
            if(jdata === undefined) {
                console.log('No data from control service');
            }
            else {
                console.log('got ' + jdata + "from cs");
            }
            data = {
            "status": {
                    "code": 200,
                    "message": 'Step Motor Message received by controlservice',
                    "state":   'ok'
            }
            };
            device.device.response.send(data);
    });
}
