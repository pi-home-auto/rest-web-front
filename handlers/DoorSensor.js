//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

// DoorSensor.js -- information about state of the door
var request = require("request");
var doorState;
var spns;

exports.query = function() {
var caps = {
	"name": "DoorSensor",
	"class": "",
	"capabilities": {
	    "init":  init,
	    "write": write,
        "read" : read,
        "ioctl" : ioctl
		}
    };

    return caps;
};

function init() {

    console.log('Initialize DoorSensor');
    doorState = 'Unknown';
}


function read(device,na,na,callback) {
    console.log('The door State ' + doorState);
    request.get("http://"+device.device.addresses+":5000/doorstate",function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("door status will be sent to phone");
        }
        if(error) {
            console.log("error occurred" + error + " could not get door state");
            doorState = error;
        }
    });
    device.device.response.send(doorState);
}

function ioctl(spnsObj) {
    console.log("setting spns object in doorsensor");
    spns =  spnsObj;
}

function write(device,value,na,callback) {
    console.log('In door sensor Write');
    console.log(value);
    valueJson = JSON.parse(value);
    if (valueJson.action === "getLogDump") {
        request.get("http://"+device.device.addresses+":5000/getlogdump",function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("Logdump is progress");
            device.device.response.send('Logdump in progress');
        }
        if(error) {
            console.log("Error occurred" + error + " could not start logdump on doorsensor");
            device.device.response.send('Could not start logdump');
        }
        });
        return;
    }
    else if (valueJson.action === "setNoiseThreshold") {
        request.get("http://"+device.device.addresses+":5000/setnoisethreshold/"+valueJson.pnt+"/"+valueJson.nnt,function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("Setting noise threshold");
            device.device.response.send('Noise threshold set');
        }
        if(error) {
            console.log("Error occurred" + error + " could not set noise threshold values");
            device.device.response.send('Could not set NoiseThreshold');
        }
        });
        return;
    }
    else if(valueJson.action === "setDoorState") {
        var doorStateData = valueJson.state.split(" ");
        var doorState = doorStateData[0];
        var deg = 0;

        if (doorState === 'midway') {
            deg = Math.round(doorStateData[1]);
        }
        else if (doorState === "open") {
            deg = 94;
        }

        if(spns) {
            spnsData = {device:device.device.uuid,deviceType:device.device.type,state:doorState,degrees:deg,timestamp:valueJson.timestamp,event:valueJson.event};
            spns.send(JSON.stringify(spnsData));
            console.log("sent notification"  + spns);
        }
        device.device.response.send('spns sending');
    }
    else{
        device.device.response.send('Unrecognized message');
    }
}
