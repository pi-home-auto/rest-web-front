//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

// DoorOpener.js -- handle radio commands to open/close garage door remotely

var control = require('../control');


exports.query = function() {
    var caps = {
	"name": "DoorOpener",
	"class": "",
	"capabilities": {
	    "ioctl": ioctl
        }
    };

    return caps;
}

/**
 * Get or Set a parameter on the door opener.
 * If param is an array then Set param[0] to param[1] (WRITE).
 * If param is a string then Get param and return (READ).
 */
function ioctl(device, param) {
    var retval;
    var json = { "handler": "garage" };

    if (typeof param === 'string') {
        json["method"] = "readkey";
	json["param1"] = param;    // name
    } else {
        json["method"] = "writekey";
	json["param1"] = param[0]; // name
	json["param2"] = param[1]; // value
    }

    var ctrljson = control.wrapCommand(json);
    console.log('DoorOpener send: ' + JSON.stringify(ctrljson));
    control.send(ctrljson, function(jdata) {
        console.log('DoorOpener callback got message');
        if (device.device.response) {
            console.log('DoorOpener sending response');
            if (jdata === undefined) {
                jdata = {
	            "status": {
                        "code": 404,
                        "message": 'No data returned from controlservice',
                        "state":   'error'
	            }
                };
            }
            device.device.response.send(jdata);

        } else {
            console.log('No response set for DoorOpener');
        }
        retval = jdata;
    });

    return retval;
}
