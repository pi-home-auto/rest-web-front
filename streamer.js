//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

var baseDb = require('./database');
var http = require('http');
var fs = require('fs');
var exec = require('child_process').exec;


/*
 * This returns the URL that the user can stream
 */
exports.getStreamUrl = function(request, response) {
    // /camera/:devuuid/stream/:vidfmt
    console.log('my address: ' + request.protocol + ', ' + request.headers.host + ', ' + request.ip);
    var jsonData;
    var url;
    var vidfmt = request.params.vidfmt;
    var host = request.headers.host.split(":");
    if (vidfmt == 'mjpeg') {
	url = request.protocol + '://' + host[0] + ':8088/stream_simple.html';
	jsonData = { stream: "Live",
		     videoFormat: "mjpeg",
		     resolution: "320x240",
		     fps: 8,
		     url: url
		   };
    } else if (vidfmt == 'h264') {
	url = 'rtp://:9000';
	jsonData = { stream: "Live UDP",
		     videoFormat: "h264",
		     resolution: "800x420",
		     fps: 10,
		     url: url,
                     host: host[0]
		   };
    } else {
	baseDb.sendJsonStatus(response, 400, "Invalid video format " + vidfmt, "error");
	return;
    }
	
    response.send(jsonData);
}

/*
 * Return the filename of the last (or current) video recording
 */
exports.getLastRecording = function(request, response) {
    var recfile = '/home/pi/.recordstream.cfg';
    fs.readFile(recfile, 'utf8', function(err, filename) {
        var jsonData;
        if (err) {
            var msg = 'Cannot open ' + recfile;
            console.log(msg);
            jsonData = { "status": {
                "code": 404,
                "message": msg,
                "state": "error"
            } };
        } else {
            var msg = 'Video recording in ' + filename;
            console.log(msg);
            jsonData = { "status": {
                "code": 200,
                "message": msg,
                "state": "OK",
                "filename": filename
            } };
        }
        response.send(jsonData);
    });
}

// Stream an MP4 file via http
exports.stream = function(fileName, request, response) {
    var stat = fs.statSync(fileName);
    var total = stat.size;

    if (request.headers['range']) {
	var range = request.headers.range;
	var parts = range.replace(/bytes=/, "").split("-");
	var partialEnd = parts[1];

	var start = parseInt(parts[0]);
	var end = partialEnd ? parseInt(partialEnd) : total - 1;
	var chunkSize = end - start + 1;
	console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunkSize);

	var file = fs.createReadStream(fileName, { start: start, end: end });
	response.writeHead(206, { 'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
				  'Accept-Ranges': 'bytes', 'Content-Length': chunkSize,
				  'Content-Type': 'video/mp4' });
	file.pipe(response);
    } else {
	console.log('ALL: ' + total);
	response.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4' });
	fs.createReadStream(fileName).pipe(response);
    }
}
