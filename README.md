# rest-web-front
## REST and Web Frontend

A node.js server that provides both REST and web frontend.
It can be coupled directly with the [controlservice](https://gitlab.com/pi-home-auto/controlservice)
that is part of the Pi Home Automation group.

## Usage

First refer to the [Sample Projects](https://gitlab.com/pi-home-auto/sample-projects) project that describes how to configure and install the controlservice and this node.js server.

You need to set the HA_BASE_DIR environment variable to your installation's home automation root. Also, the HA_DBFILE variable should name the sqlite database to use.  You can set these at the shell prompt or better, set the variable in your $HOME/.profile file. Here's an example:

    export HA_BASE_DIR=/home/pi/homeauto
    export HA_DBFILE=simple.db

The HA_BASE_DIR is where the home automation is installed and the node.js code is in $HA_BASE_DIR/nodejs.

First, start the controlservice:

    controlservice -b

On a separate terminal, start the node.js server:

    export NODE_PATH=${HA_BASE_DIR}/node_modules
    cd $HA_BASE_DIR/nodejs
    npm start

## NOTE

This project needs more documentation and features to be useful:
* It relies on a SQL database but does not provide a loader.
* There is also some configuration necessary.
* Depends on a specific protocol to communicate with the controlservice.

The camera page should support multiple cameras. It currently only supports 1 camera on
the current module.

The camera page has a link to show streaming video in an iframe. To enable streaming video, follow the
instructions at [Raspberry Pi Webcam Server](https://pimylifeup.com/raspberry-pi-webcam-server). This
streams video from the Pi's camera to the localhost at TCP/IP port 8081.

Further, there is a lot that will be added such as TLS, better web pages, etc.

## TODO

* A gpio is referred by its pin number, but should refer to a sensor number, where the first gpio used is 1, the second 2, etc. and gpio 0 is reserved for the status LED. Also, allow naming gpio's.
* Add audio devices to sounds page, camera devices to camera page so you can select which device to use.
