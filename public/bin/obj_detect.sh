#!/bin/bash

CAMERA_UUID='CAM-123-1'
DOWNLOADDIR="$HOME/nodejs/HeidiServer/public/files"
DATABASEFILE="$HOME/nodejs/databases/heidiplan.db"
IMGFILE="$DOWNLOADDIR/objdetect.jpg"

TMPFILE=/tmp/zzobjdetect$$

export RECOGDIR="$HOME/bin"
export PATH="$RECOGDIR:$PATH"

moduleUrl=$(sqlite3 $DATABASEFILE <<EOF
select module_url from configuration where name="camera";
EOF
)
CAMERA_PI=$(echo $moduleUrl | sed -e 's%http://%%' )
echo "The camera ip address is $CAMERA_PI"

# Call camera pi to take a snapshot with resolution 648*486
T="$(date +%s)"
curl -s http://$CAMERA_PI/camera/$CAMERA_UUID/'snapshot?width=648&height=486' -o $TMPFILE -X POST >/dev/null
DLFILE=$(grep snapshot $TMPFILE | sed -e 's/^.*: "//' -e 's/",//')
T="$(($(date +%s)-T))"
echo "snapshot time in seconds: ${T}"

# Download snapshot from camera pi
T="$(date +%s)"
curl -s http://$CAMERA_PI/media/image/$DLFILE -o $IMGFILE >/dev/null
T="$(($(date +%s)-T))"
echo "download time in seconds: ${T}"

#tempoararily remove this deletion action due to delete REST call has been correctly implemented.
echo "download completed. delete file $DLFILE"
curl -s http://$CAMERA_PI/media/image/$DLFILE -X DELETE

cd "$RECOGDIR"

# Now, do image detection
T="$(date +%s)"
objectDetection $IMGFILE
T="$(($(date +%s)-T))"
echo "objectDetection time in seconds: ${T}"
