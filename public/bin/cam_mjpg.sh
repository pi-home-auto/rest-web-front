#!/bin/bash
exec 2>&1 >/tmp/cam_mjpg.log

FPS=8
VIDSIZE='320x240'

if [ $# -eq 1 ]; then
    FPS=$1
    echo 'Set fps to' $FPS
elif [ $# -eq 3 ]; then
    FPS=$1
    WIDTH=$2
    HEIGHT=$3
    VIDSIZE="${WIDTH}x${HEIGHT}"
    echo 'Set video: fps='$FPS "(${VIDSIZE})"
elif [ $# -ne 0 ]; then
    echo "Usage: $0 [ fps [ width height ] ]"
    exit 2
fi

echo 'Start mjpg streamer as' $(id)
camera_control start mjpeg $FPS $WIDTH $HEIGHT
sleep 2
# mjpg_streamer -i "/usr/local/lib/input_testpicture.so" -o '/usr/local/lib/output_http.so -p 8088 -w ../www-mjpeg'
/usr/local/bin/mjpg_streamer -i "/usr/local/lib/input_uvc.so -r $VIDSIZE -f $FPS -l blink" -o '/usr/local/lib/output_http.so -p 8088 -w /home/pi/www-mjpeg' >/tmp/cam_mjpg2.log 2>&1
exit $?
