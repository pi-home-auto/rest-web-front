#!/bin/bash

DOWNLOADDIR="$HOME/nodejs/HeidiServer/public/files"
REFIMAGE="$DOWNLOADDIR/referenceImage.jpg"
XMLFILE="$DOWNLOADDIR/objectImage_center.xml"

function die {
    echo "$1"
    exit 2
}

[ -e $REFIMAGE ] || die "File $REFIMAGE does not exist"
[ -e $XMLFILE ] || die "File $XMLFILE does not exist"

export RECOGDIR="$HOME/bin"
export PATH="$RECOGDIR:$PATH"
cd "$RECOGDIR"

T="$(date +%s)"
getTemplateImage $REFIMAGE $XMLFILE
T="$(($(date +%s)-T))"
echo "getTemplateImage time in seconds: ${T}"
