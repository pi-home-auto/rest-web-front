#!/bin/bash
exec 2>&1 >/tmp/cam_h264.log

RECFILE=/home/pi/.recordstream.cfg
STATFILE=/home/pi/.stream.cfg
#WIDTH=426
#HEIGHT=240
# my original defaults
FPS=10
WIDTH=800
HEIGHT=420
PORT=9000
#HEIGHT=600
# SD is FPS=30 WIDTH=480 HEIGHT=360

# start out clean
killall gst-launch-1.0

if [ $# -gt 1 -a "$1" = "-r" ]; then
    Record=y
    shift
else
    Record=n
fi

if [ $# -eq 2 ]; then
    FPS=$2
    echo 'Set video: fps='$FPS
elif [ $# -eq 4 ]; then
    FPS=$2
    WIDTH=$3
    HEIGHT=$4
    echo 'Set video: fps='$FPS "(${WIDTH}x${HEIGHT})"
elif [ $# -eq 5 ]; then
    FPS=$2
    WIDTH=$3
    HEIGHT=$4
    PORT=$5
    echo 'Set video: fps='$FPS "(${WIDTH}x${HEIGHT}) port=$PORT"
elif [ $# -ne 1 ]; then
    echo "Usage: $0 [ fps [ width height ] ]"
    exit 2
fi
HOST=$1

echo "$WIDTH $HEIGHT $FPS" > $STATFILE

if [ $Record = n ]; then
    #gst-launch-1.0 -v rpicamsrc preview=false bitrate=2000000 video-stabilisation=true ! video/x-h264,width=$WIDTH,height=$HEIGHT,framerate=$FPS/1 ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=$HOST port=9000
    gst-launch-1.0 -v rpicamsrc preview=false bitrate=2000000 video-stabilisation=true ! tee name=split ! queue ! video/x-h264,width=$WIDTH,height=$HEIGHT,framerate=$FPS/1 ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=$HOST port=$PORT split. ! queue ! decodebin ! videorate ! video/x-raw,framerate=1/1 ! jpegenc ! multifilesink location=/home/pi/snapshot.jpg
    #gst-launch-1.0 -v rpicamsrc preview=false bitrate=2000000 video-stabilisation=true ! tee name=split ! queue ! video/x-h264,width=$WIDTH,height=$HEIGHT,framerate=$FPS/1 ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=$HOST port=9000 split. ! queue ! decodebin ! video/x-raw,width=160,height=84 ! jpegenc ! filesink location=/home/pi/snapshot.jpg
else
    VID=vid-$(date +'%F-%H%M%S').h264
    OUTFILE=/home/pi/nodejs/HeidiServer/public/videos/$VID
    echo -n $VID > $RECFILE
    curl -s http://localhost:8888/media/video/$VID -X PUT > /dev/null
    gst-launch-1.0 -v rpicamsrc preview=false bitrate=2000000 video-stabilisation=true ! video/x-h264,width=$WIDTH,height=$HEIGHT,framerate=$FPS/1 ! tee name=split ! queue ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=$HOST port=$PORT split. ! queue ! filesink location=$OUTFILE
fi

exit $?
