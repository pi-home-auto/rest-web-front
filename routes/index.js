//
//   Copyright 2020-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner

/*
 * GET home page
 */
exports.index = function(req, res, temp, moduleName) {
    if (req.device.type == 'phone') {
        res.render('mindex', { title: 'Train Central Control', temp: temp, module: moduleName, devType: req.device.type });
    } else {
        res.render('index', { title: 'Train Central Control', temp: temp, module: moduleName, devType: req.device.type });
    }
};

/*
 * Show page with list of media
 */
exports.media = function(response, mediatype, medialist) {

    medialist.type = mediatype;
    medialist.title = 'Homeauto Server Media List';
    response.render('media', medialist);
};

exports.sound = function(response, mediatype, medialist) {

    medialist.type = mediatype;
    medialist.title = 'Homeauto Server Sound List';
    response.render('sound', medialist);
};

/*
 * Camera detail webpage
 */
exports.camera = function(request, response, json) {
    json.title = 'Camera Information';
    response.render('camera', json);
};

exports.stream = function(request, response, json) {
    json.title = "Camera Live Stream";
    response.render('stream', json);
};

exports.devices = function(request, response, json) {
    json.title = 'Device List';
    response.render('devices', json);
};

exports.lights = function(request, response, json) {
    json.title = 'Train Central Control';
    response.render('lights-m', json);
};

exports.pictures = function(request, response, json) {
    json.title = 'Train Central Control';
    response.render('pictures-m', json);
};

exports.sounds = function(request, response, json) {
    json.title = 'Train Central Control';
    response.render('sounds-m', json);
};

exports.turnouts = function(request, response, json) {
    json.title = 'Train Central Control';
    response.render('turnouts-m', json);
};
