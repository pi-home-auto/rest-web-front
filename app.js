//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner
//
//   app.js  Main node.js application

/**
 * Module dependencies.
 */
var bodyParser = require('body-parser');
var control = require('./control');
var device = require('express-device');
var errorHandler = require('errorhandler');
var express = require('express');
var favicon = require('favicon');
var fs = require('fs');
var http = require('http');
var path = require('path');
var handler = require('./handlers/defines');
var baseDb = require('./database');
var exec   = require('child_process').exec;
var reqmod = require('request');
var routes = require('./routes');
var spawn  = require('child_process').spawn;
var stream = require('./streamer');
var user = require('./routes/user');
//var notification = require('./notification');

var db = baseDb.open();
var app = express();

console.log('Running arch=' + process.arch);

// all environments
app.set('port', process.env.PORT || 8888);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
//app.use(favicon('public/images/favicon32.gif', function(err, favicon_url) {
//    if (err != null) {
//        console.log('Error loading favicon');
//    }
//}));
//app.use(express.logger('dev'));
app.use(require('morgan')('combined'));
app.use(express.json());
app.use(express.urlencoded());
app.use(bodyParser({ uploadDir: path.join(__dirname, 'public/uploads') }));
//app.use(express.methodOverride());
app.use(device.capture());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// device.enableViewRouting(app);

var myModule;
var strChild;
var detectTimer;
var streamingState = 'na';

var handlersRequireNotification = ['DoorSensor','KnockSensor'];
// notification.getNotificationData(notificationReadCallback);  // try to read peristed uuid

baseDb.initialize(db, function(myConfig) {
    control.initialize(myConfig);
    myModule = myConfig.name;
    console.log('My module name is ' + myModule);
});

var temperature = 'Unknown';
function checkTemp() {
    exec('vcgencmd measure_temp', function(err, stdout, stderr) {
        if (err === null) {
            temperature = stdout;
            if (temperature.substr(0,5) === 'temp=') {
                temperature = temperature.substr(5).replace("'", "&deg;");
            }
        } else {
            console.log('Cannot get temperature: ' + stderr);
        }
    });
}
setImmediate(checkTemp);	// get the current temperature
setInterval(checkTemp, 60000);	// and refresh it every minute
setTimeout(function() {
    console.log('CPU temperature is ' + temperature);
}, 2000);


function notificationReadCallback() {
    console.log("In notificationReadCallback");
    var spuser = notification.checkNotificationData();
    if (spuser !== "") {
        setupNotification();
    }
}

// start scheduled detection, if it was running
fs.readFile('.schedule', 'utf8', function(err, interval) {
        if (err) {
            console.log('No schedule for detector');
        } else {
            console.log('Detector schedule from file: ' + interval);
            if (interval > 1) {
                detectTimer = setInterval(runObjectDetect, interval * 1000);
            }
        }
});


// development only
if ('development' === app.get('env')) {
    console.log('Setting up development env.');
    app.use(errorHandler());
    app.locals.pretty = true;
}

// Handlers
function handleIndex(request, response) {
    routes.index(request, response, temperature, myModule);
}

function setupNotification() {
    control.setNotification(notification);	// set spns for control service

    // put spsns object in handler.
    for (var i = 0; i < handlersRequireNotification.length; i++) {
        var hdlr = baseDb.getHandler(handlersRequireNotification[i]);
        var caps = hdlr.caps.capabilities;
        if(caps[handler.IOCTL]) {
            caps[handler.IOCTL](notification);
        }
        else
            console.log("IOCTL method not defined for handler:" + handlersRequireNotification[i]);
    }
}

function handlePutSpnsUser(request, response) {
    // /user/spnsuuid/:uuid
    var notiType = "spns";
    var spnsUuid = request.params.uuid;
    var json = { "handler": "notification" };
    var ctrljson = {};
    console.log('The notification type is ' + notiType + ' notificationid is:'  +spnsUuid);
    notification.setNotificationData(notiType,spnsUuid);
    setupNotification();
    console.log('The spnsuuid is ' + spnsUuid);
    json.method = "setSpns";
    ctrljson = control.wrapCommand(json);
    console.log('Notification type send: ' + JSON.stringify(ctrljson));
    control.send(ctrljson, function(jdata) {
        console.log('got ' + jdata + "from cs");
    });
    baseDb.sendJsonStatus(response, 200, spnsUuid + " will get push notifications", "OK");

}

function handlePutNotificationType(request,response) {
    var notiType = request.params.notificationtype;
    var json = { "handler": "notification" };
    var ctrljson = {};
    console.log('The notification type is ' + notiType + ' notificationid is:'  +request.params.notificationid);

    if(request.params.portnumber == undefined) {
        notification.setNotificationData(notiType,request.params.notificationid);
    }
    else {
	console.log('The portnumber is:' + request.params.portnumber);
	notification.setNotificationData(notiType, request.params.notificationid + ':' + request.params.portnumber);
    }
    setupNotification();
    if(notiType == 'spns') {
        var spnsUuid = request.params.notificationid;
        console.log('The spnsuuid is ' + spnsUuid);
        json.method = "setSpns";
        ctrljson = control.wrapCommand(json);
        console.log('Notification type send: ' + JSON.stringify(ctrljson));
        control.send(ctrljson, function(jdata) {
            console.log('got ' + jdata + "from cs");
        });

        baseDb.sendJsonStatus(response, 200, spnsUuid + " will get push notifications", "OK");
    } else if (notiType == 'tcp') {
        var ip = request.params.notificationid;
	var port = request.params.portnumber;
        console.log('The ipnport is ' + ip + ':' + port );

        json.method = "setTcp";
        json.ipaddress = ip;
	json.port = parseInt(port);
        ctrljson = control.wrapCommand(json);
        console.log('Notification type send: ' + JSON.stringify(ctrljson));
        control.send(ctrljson, function(jdata) {
            console.log('got ' + jdata + "from cs");
        });

        baseDb.sendJsonStatus(response, 200, ip + ':' + port + " will get push notifications", "OK");
    }
    else
        baseDb.sendJsonStatus(response, 404, "notification type not supported", "error");
}


function handleRunDetect(request, response) {
    // /detect/run/:prognum
    var prognum = request.params.prognum;
    var cmd;
    if (prognum == 1) {
        cmd = 'get_template.sh';
    } else if (prognum == 2) {
        cmd = 'obj_detect.sh';
    } else if (prognum == 3) {
	runObjectDetect();
	baseDb.sendJsonStatus(response, 200, "object detection program has been triggered", "OK");
	return;
    } else {
        baseDb.sendJsonStatus(response, 400, "Program must be 1 or 2 or 3", "error");
        return;
    }
    console.log('Going to run detect program ' + cmd);
    exec(cmd, function(err, stdout, stderr) {
        if (err == null) {
            console.log('detect program returns: ' + stdout);
            if (prognum == 2) {
                var msgs = stdout.split("\n");
                var status = stdout;
                var distance = '';
                for (var idx in msgs) {
                    if (msgs[idx].indexOf('distance = ') > -1) {
                        distance = msgs[idx];
                    } else if (msgs[idx].indexOf('Opened') > -1) {
                        status = msgs[idx];
                    } else if (msgs[idx].indexOf('Closed') > -1) {
                        status = msgs[idx];
                    }
                }
                var jsonData = { message: 'Object detection program',
                                 status: status,
                                 distance: distance
                               };
                response.send(jsonData);
            } else {
                var msgs = stdout.split("\n");
                var xRes = '0';
                var yRes = '0';
                for (var idx in msgs) {
                    if (msgs[idx].indexOf('pointX=') > -1) {
                        var resX = msgs[idx].split("=");
                        xRes = resX[1];
                    } else if (msgs[idx].indexOf('pointY=') > -1) {
                        var resY = msgs[idx].split("=");
                        yRes = resY[1];
                    }
                }
                var jsonData = { message: 'object detected!',
                                 x: xRes,
                                 y: yRes
                               };
                response.send(jsonData);
            }
        } else {
            var msg = 'Error running detection program: ' + stderr;
            baseDb.sendJsonStatus(response, 400, msg, "error");
        }
    });
}

function handleRunUploadDetect(request, response) {
    // /detect/uploadrun/:filename?
    handlePostFiles(request, response, function() {
        request.params.prognum = 1;
        handleRunDetect(request, response);
    });
}

function runObjectDetect() {
    console.log('Going to run object detection program');
    exec('obj_detect.sh', function(err, stdout, stderr) {
        if (err == null) {
            console.log('detection program returns: ' + stdout);
            if (stdout.indexOf('Opened') > -1) {
                // send spns message
                var doorEvent = {
                    "device": "door_camera",
                    "deviceType": "detector",
                    "state": "Opened"
                };
                notification.send(JSON.stringify(doorEvent));
            }
            else if (stdout.indexOf('Closed') > -1 ) {
                // send spns message
                var doorEvent = {
                    "device": "door_camera",
                    "deviceType": "detector",
                    "state": "Closed"
                };
                notification.send(JSON.stringify(doorEvent));
            }
        } else {
            console.log('Error running detection program: ' + stderr);
        }
    });
}

function handleScheduleDetect(request, response) {
    // /detect/schedule/:interval
    if (request.params.interval == undefined) {
        baseDb.sendJsonStatus(response, 400, 'Must specify an interval (in seconds).', "error");
    } else {
        var interval = parseInt(request.params.interval);
        if (interval < 1) {
            if (detectTimer) {
                console.log('Stopping detection program');
                clearInterval(detectTimer);
                detectTimer = undefined;
                baseDb.sendJsonStatus(response, 200, 'Detection program has been stopped', "OK");
            } else {
                baseDb.sendJsonStatus(response, 404, 'Detection program was not scheduled', "error");
            }
            fs.writeFileSync('.schedule', '0');
        } else {
            var msg = 'Detection scheduled every ' + interval + ' seconds.';
            if (detectTimer) {
                clearInterval(detectTimer);
            }
            detectTimer = setInterval(runObjectDetect, interval * 1000);
            baseDb.sendJsonStatus(response, 200, msg, "OK");
            fs.writeFileSync('.schedule', interval);
        }
    }
}

function handleCameraGetPicture(request, response) {
    // /camera/:devuuid/getpicture [ ?width=#&height=# ]
    var uuid = request.params.devuuid;
    var cmd = 'snapshot';
    if (request.query.width)  cmd += ' ' + request.query.width;
    if (request.query.height) cmd += ' ' + request.query.height;
    console.log('download a picture with camera ' + uuid + ' with: ' + cmd);
    exec(cmd, function(err, stdout, stderr) {
        if (err == null) {
            var pic = stdout.replace('\n', '');
            console.log('snapshot returns: ' + pic +
                        '\n stderr: ' + stderr);
            var file = __dirname + '/public/images/' + pic;
            response.sendfile(file);
        } else {
            var errmsg = 'Unable to take snapshot: ' + stderr;
            baseDb.sendJsonStatus(response, 404, errmsg, "error");
        }
        //spawn('cam_h264.sh', []);
    });
}

function handleCameraSnapshot(request, response) {
    // /camera/:devuuid/snapshot [ ?width=#&height=# ]
    var uuid = request.params.devuuid;
    var cmd = 'snapshot';
    if (request.query.width)  cmd += ' ' + request.query.width;
    if (request.query.height) cmd += ' ' + request.query.height;
    console.log('take a snapshot on camera ' + uuid + ' with: ' + cmd);
    exec(cmd, function(err, stdout, stderr) {
        if (err == null) {
            var pic = stdout.replace('\n', '');
            console.log('snapshot returns: ' + pic +
                        '\n and: ' + stderr);
            var jsonData = {
                snapshot: pic,
                status: 'uploaded'
            };
            response.send(jsonData);
        } else {
            var errmsg = 'Unable to take snapshot: ' + stderr;
            baseDb.sendJsonStatus(response, 404, errmsg, "error");
        }
        //spawn('cam_h264.sh', []);
    });
}

function handleCameraVideoClip(request, response) {
    // /camera/:devuuid/videoclip [ ?time=# ]
    var uuid = request.params.devuuid;
    var args = [];
    if (request.query.time && !isNaN(request.query.time)) {
        var dur = Number(request.query.time) * 1000;	// convert to millisec.
        args.push(dur);
    }
    console.log('take a video clip on camera ' + uuid);
    var strChild = spawn('videoclip', args);
    if (strChild.pid > 0) {
        baseDb.sendJsonStatus(response, 200, 'Started videoclip, pid=' + strChild.pid, "OK");
    } else {
        baseDb.sendJsonStatus(response, 404, "Unable to capture videeo", "error");
    }
}

function handleCameraControl(request, response) {
    // /camera/:devuuid/control/:value [ ?width=#&height=#&fps=#&port=# ]
    var uuid = request.params.devuuid;
    var value = request.params.value;
    if (value == 0) {
        console.log('stop streamer ' + ':' + value);
        exec('camera_control stop h264', function(err, stdout, stderr) {
            if (err == null) {
                baseDb.sendJsonStatus(response, 200, "Stopped: " + stdout, "OK");
            } else {
                baseDb.sendJsonStatus(response, 404, "Unable to stop stream: " + stderr, "error");
            }
        });
    } else {
        exec('camera_control start h264', function(err, stdout, stderr) {
            if (err == null) {
                baseDb.sendJsonStatus(response, 200, "Started: " + stdout, "OK");
            } else {
                baseDb.sendJsonStatus(response, 404, "Unable to start stream: " + stderr, "error");
            }
        });
    }
/*
        var script = (vidfmt == 'h264') ? 'cam_h264.sh' : 'cam_mjpg.sh';
        var args = [];
        if (value > 1) args.push('-r');		// record
        if (vidfmt == 'h264') args.push(request.ip);
        if (request.query.fps) args.push(request.query.fps);
        if (request.query.width) args.push(request.query.width);
        if (request.query.height) args.push(request.query.height);
        if (request.query.port) args.push(request.query.port);
        var strChild = spawn(script, args);
        console.log('script running at ' + strChild.pid);
        if (strChild.pid > 0) {
            if (value > 1) {	// Recording -- return filename in json status
	        setTimeout(stream.getLastRecording, 700, request, response);
            } else {
                baseDb.sendJsonStatus(response, 200, "Started", "OK");
            }
        } else {
            baseDb.sendJsonStatus(response, 404, "Unable to control stream", "error");
        }
    }
*/
}

function handleCameraPage(request, response) {
    // /camera.html?uuid=xxx
    var uuid = request.query.uuid;
    baseDb.readDevice(db, uuid, function(cameraJson) {
        routes.camera(request, response, cameraJson);
    });
}

function handleCameraStream(request, response) {
    // /stream.html?uuid=xxx&vidfmt=yyy
    var uuid = request.query.uuid;
    baseDb.readDevice(db, uuid, function(cameraJson) {
        routes.stream(request, response, cameraJson);
    });
/*
    //var idx = (request.query.vidfmt == 'h264') ? 1 : 0;
    var vidfmt = request.query.vidfmt;
    baseDb.readDevice(db, uuid, function(json) {
        callHandler(json, handler.IOCTL, [ "vidfmt", vidfmt ]);
        callHandler(json, handler.READ, null, null, function(url) {
            if (url) {
                var host = request.headers.host.split(":");
                url = url.replace("@HOST@", host[0]);
                console.log('redirect to ' + url);
                response.redirect(url);
            } else {
                baseDb.sendJsonStatus(response, 400, "Cannot stream from device " + uuid, "error");
            }
        });
    });
*/
}

function sendResponse(request, response) {
    console.log('send response now');
    stream.getStreamUrl(request, response);
}
function handleCameraGetStream(request, response) {
    // '/camera/:devuuid/stream/:vidfmt'
    var uuid = request.params.devuuid;
    var vidfmt = request.params.vidfmt;
    forwardToModule(request, response, uuid, function(json) {
        if (vidfmt == 'h264') {
            spawn('cam_h264.sh', [ request.ip ]);
        } else {
            spawn('cam_mjpg.sh', []);
        }
	setTimeout(sendResponse, 1000, request, response);
        //TODO rewrite this to do callHandler(READ)
        //stream.getStreamUrl(request, response);
    });
}

function handleCameraShowStream(request, response) {
    // /camera/:devuuid/showstream/:vidfmt
    var uuid = request.params.devuuid;
    forwardToModule(request, response, uuid, function(json) {
        spawn('cam_h264.sh', [ request.ip ]);
        stream.findStreamUrl(request, function(jdata) {
            if (jdata !== undefined) {
                var streamEvent = {
                    "device": uuid,
                    "deviceType": "videostream",
                    "uri": jdata.url
                };
                if (spns) {
                    spns.send(JSON.stringify(streamEvent));
                } else {
                    console.log('Error: spns not set in node app.js');
                }
            }
            baseDb.sendJsonStatus(response, 200, "OK", "OK");
        });
    });
}

function handleDevice(request, response) {
    var uuid = request.params[0];
    forwardToModule(request, response, uuid, function(json) {
        response.send(json);
    });
}

function handleDevices(request, response) {
    baseDb.getDevicesList(db, function(json) {
        appendStatus(json);
        response.send(json);
    });
}

function handleDevicesType(request, response) {
    var devtype = request.params.devtype;
    baseDb.getDevicesList(db, function(json) {
        appendStatus(json);
        response.send(json);
    }, devtype);
}

function handleDevicesPage(request, response) {
    baseDb.getDevicesList(db, function(json) {
        appendStatus(json, request, response, function(request, response, json) {

            routes.devices(request, response, json);
        });
    });
}

function handleLightsPage(request, response) {
    baseDb.getDevicesList(db, function(json) {
        appendStatus(json, request, response, function(request, response, json) {
            routes.lights(request, response, json);
        });
    }, "", "light");
}

function handleTurnoutsPage(request, response) {
    baseDb.getDevicesList(db, function(json) {
        appendStatus(json, request, response, function(request, response, json) {
            routes.turnouts(request, response, json);
        });
    }, "", "turnout");
}

function appendStatus(json, request, response, callback) {
    // Note: Forwarding requests to other modules here
    //       can generate many requests/responses
    var cbcount = 0;
    for (var i = 0; i < json.devices.length; i++) {
        /*forwardCollect(request, json.devices[i], json.);
        callHandler(json.devices[i], handler.READ, null, null, function(ret) {
            if (ret == undefined) {
                ret = '-';
            }
            json.devices[i].device.setting = ret;
        });*/
        var dev = json.devices[i];
        var devtypes = [ 'input', 'switch', 'pwm', 'servo' ];
        if (dev.device.module == myModule && (devtypes.indexOf(dev.device.type) > -1)) {
            ++cbcount;
            callHandler(dev, handler.READ, null, null, function(ret) {
                console.log('Function got reply: ' + ret.reply.param2);
                if (ret && ret.reply) {
                    dev.device.setting = ret.reply.param2;
                }
                --cbcount;
                if (cbcount == 0 && callback != undefined) {
                    callback(request, response, json);
                }
            });
        }
    }
}

function handleDeviceGetKey(request, response) {
    // /device/:devuuid/key/:key
    var uuid = request.params.devuuid;
    forwardToModule(request, response, uuid, function(json) {
        var key = request.params.key;
        json.device.response = response;
        callHandler(json, handler.IOCTL, key);
    });
}

function handleDevicePutKey(request, response) {
    // /device/:devuuid/key/:key/:value
    var uuid  = request.params.devuuid;
    forwardToModule(request, response, uuid, function(json) {
        var key   = request.params.key;
        var value = request.params.value;
        json.device.response = response;
        callHandler(json, handler.IOCTL, [ key, value]);
    }, 'PUT');
}

/* GetSettings and PutSettings treat a device as if it only has a single setting.
 * You could send/receive json to set/get multiple settings on a device, but the new
 * GetKey, PutKey should be used instead
 */
function handleDeviceGetSetting(request, response) {
    // lookup device UUID, then query device state
    var uuid = request.params[0];
    forwardToModule(request, response, uuid, function(json) {
        json.device.response = response;
        callHandler(json, handler.READ, null, null, function(ret) {
            console.log('callback from callHandler from getSettings ' + JSON.stringify(ret));
            response.send(ret);
/*            if (ret == undefined) {
                ret = '-';
            }
            json.device.setting = ret;
            response.send(json); */
        });
    });
}

function handleDevicePutSetting(request, response) {
    // /device/*/setting/*
    // lookup device UUID, then set device state
    var uuid = request.params[0];
    forwardToModule(request, response, uuid, function(json) {
        var value = request.params[1];
        json.device.response = response;
        callHandler(json, handler.WRITE, value, null, function(ret) {
/*            response.send(ret); */
/*            if (ret == undefined) {
                ret = '-';
            }
            json.device.setting = ret;
            response.send(json); */
        });
    }, 'PUT');
}

function handleDevicePutMove(request, response) {
    console.log('MOVER: req=' + JSON.stringify(request.body, undefined, 4));
    // /device/*/move/*
    // lookup device UUID, then set device state
    var uuid = request.params[0];
    forwardToModule(request, response, uuid, function(json) {
        var value = request.params[1];
        json.device.response = response;
        var jval = {
            "action": "move",
            "steps": value,
            "speed": 2
        };
        callHandler(json, handler.WRITE, jval, null, function(ret) {
            response.send(ret);
        });
    }, 'PUT');
}

function handleDevicePutMoveTo(request, response) {
    console.log('MOVETO: req=' + JSON.stringify(request.body, undefined, 4));
    // /device/*/moveto/*
    // lookup device UUID, then set device state
    var uuid = request.params[0];
    forwardToModule(request, response, uuid, function(json) {
        var value = request.params[1];
        json.device.response = response;
        var jval = {
            "action": "moveTo",
            "degrees": value,
            "servo": 0
        };
        callHandler(json, handler.WRITE, jval, null, function(ret) {
            response.send(ret);
        });
    }, 'PUT');
}

function handleDeviceProgramLeds(request, response) {
    // /device/:devuuid/program/:func/:program
    // where :func = add, delete, run, stop
    // lookup device UUID, then set device state
    var uuid = request.params.devuuid;
    var func = request.params.func;
    var prog = request.params.program;
    forwardToModule(request, response, uuid, function(json) {
	json.device.response = response;
	callHandler(json, handler.WRITE, 'program', prog, function(ret) {
            response.send(ret);
	});
    }, 'PUT');
}

function handleVideos(request, response) {
    // /media/videos
    baseDb.getMediaJson(db, "video", response, function(resp, json) {
	resp.send(json);
    });
}

function handleVideosPage(request, response) {
    // /media/videos.html
    baseDb.getMediaJson(db, "video", response, function(resp, json) {
	routes.media(resp, "video", json);
    });
}

function handleImagesPage(request, response) {
    // /media/images.html
    baseDb.getMediaJson(db, "image", response, function(resp, json) {
	routes.media(resp, "image", json);
    });
}

function handlePicturesPage(request, response) {
    // /m/pictures.html
    baseDb.getMediaJson(db, "picture", response, function(resp, json) {
	routes.pictures(request, resp, json);
    });
}

function handleSound(request, response) {
    // /media/sound/:sidfile
    var fn = request.params.sidfile;
    var file = __dirname + '/public/sounds/' + fn;
    console.log('Preview sound: ' + file);
    stream.stream(file, request, response);
}

function handleSoundPlayer(request, response) {
    // /player/sound/:sidfile
    var fn = request.params.sidfile;
    var file = __dirname + '/public/sounds/' + fn;
    const uuid = 'S-AU0-SOUND-00';
    console.log('Play sound: ' + file);
    forwardToModule(request, response, uuid, function(json) {
        const file = __dirname + '/public/sounds/' + request.params.sidfile;
        json.device.response = response;
        callHandler(json, handler.OPEN, file);
    });
}

function handleSounds(request, response) {
    // /media/sounds
    baseDb.getMediaJson(db, "sound", response, function(resp, json) {
	resp.send(json);
    });
}

function handleSoundsPage(request, response) {
    // /media/sounds.html
    baseDb.getMediaJson(db, "sound", response, function(resp, json) {
	routes.sound(resp, "sound", json);
    });
}

function handleSoundsPageM(request, response) {
    // /m/sounds.html
    baseDb.getMediaJson(db, "sound", response, function(resp, json) {
	routes.sounds(request, resp, json);
    });
}

function handleVideo(request, response) {
    // /media/video/:vidfile
    var fn = request.params.vidfile;
    var file = __dirname + '/public/videos/' + fn;
    console.log('Stream video: ' + file);
    stream.stream(file, request, response);
}

function handleDeleteImage(request, response) {
    // /media/image/:imgfile
    var fn = request.params.imgfile;
    baseDb.deleteMediaFile(db, "image", fn, response);
}

function handleDeleteVideo(request, response) {
    // /media/video/:vidfile
    var fn = request.params.vidfile;
    baseDb.deleteMediaFile(db, "video", fn, response);
}

function handlePutVideo(request, response) {
    // /media/video/:vidfile
    var fn = request.params.vidfile;
    baseDb.putMediaFile(db, "video", fn, response);
}

function handleImages(request, response) {
    // /media/images
    baseDb.getMediaJson(db, "image", response, function(resp, json) {
	resp.send(json);
    });
}

function handleImage(request, response) {
    // /media/image/:imgfile
    //TODO give error if imgfile not specified or not able to open
    var fn = request.params.imgfile;
    var file = __dirname + '/public/images/' + fn;
    response.sendfile(file);
}

function handlePutImage(request, response) {
    // /media/image/:imgfile
    var fn = request.params.imgfile;
    baseDb.putMediaFile(db, "image", fn, response);
}

function handleFiles(request, response) {
    // /media/files
    baseDb.getMediaJson(db, "file", response, function(resp, json) {
	resp.send(json);
    });
}

function handleFile(request, response) {
    // /media/file/:filename
    //TODO give error if imgfile not specified or not able to open
    var fn = request.params.filename;
    var file = __dirname + '/public/files/' + fn;
    response.sendfile(file);
}

function handlePutFile(request, response) {
    // /media/file/:filename
    var fn = request.params.filename;
    baseDb.putMediaFile(db, "file", fn, response);
}

function handleDeleteFile(request, response) {
    // /media/image/:filename
    var fn = request.params.filename;
    baseDb.deleteMediaFile(db, "file", fn, response);
}

function handlePostFiles(request, response, callback) {
    // /media/files/:filename?
    var count = Object.keys(request.files.file).length;
    if (count < 1) {
        console.log('Request: ' + JSON.stringify(request.body, undefined, 4));
        baseDb.sendJsonStatus(response, 400, 'No file specified to upload', "ERROR");
    } else if (count > 1 && request.params.filename) {
        baseDb.sendJsonStatus(response, 400, 'Cannot specify filename when uploading '
                               + 'multiple files', "ERROR");
    } else {
        console.log('Uploading ' + count + ' files\n'
                    + JSON.stringify(request.files.file, undefined, 4));
        for (var x in request.files.file) {
            var fobj = request.files.file[x];
            console.log('-> ' + JSON.stringify(fobj, undefined, 4));
            var ftype = 'file';
            if (fobj.type !== undefined) {
                if (fobj.type.indexOf('image') != -1) {
                    ftype = 'image';
                } else if (fobj.type.indexOf('video') != -1) {
                    ftype = 'video';
                }
            }
            var fileName;
            // use request.params.filename if defined
            if (request.params.filename == undefined) {
                fileName = fobj.originalFilename;
            } else {
                fileName = request.params.filename;
            }
            var file = __dirname + '/public/' + ftype + 's/' + fileName;
            console.log('Uploaded: ' + fobj.path + ' -> ' + file + ' : ' + fobj.type);
            fs.rename(fobj.path, file, function(err) {
                if (err) {
                    console.log('ERROR: could not rename to ' + file);
                } else {
                    baseDb.putMediaFile(db, ftype, fileName);     // update db
                }
            });
        }
        if (callback !== undefined) {
            callback();
        } else {
            baseDb.sendJsonStatus(response, 200, 'Uploaded ' + count + ' files', "OK");
        }
    }
}
/**** This one works from curl, but not Apache HttpPost of MultipartEncoder
function handlePostFiles(request, response) {
    // /media/files/:filename?
    var count = Object.keys(request.files).length;
    if (count < 1) {
        console.log('Request: ' + JSON.stringify(request.body, undefined, 4));
        baseDb.sendJsonStatus(response, 400, 'No file specified to upload', "ERROR");
    } else if (count > 1 && request.params.filename) {
        baseDb.sendJsonStatus(response, 400, 'Cannot specify filename when uploading '
                               + 'multiple files', "ERROR");
    } else {
        console.log('Uploading ' + count + ' files\n'
                    + JSON.stringify(request.files, undefined, 4));
        for (var x in request.files) {
            var fobj = request.files[x];
            console.log('-> ' + JSON.stringify(fobj, undefined, 4));
            var ftype = 'file';
            if (fobj.type !== undefined) {
                if (fobj.type.indexOf('image') != -1) {
                    ftype = 'image';
                } else if (fobj.type.indexOf('video') != -1) {
                    ftype = 'video';
                }
            }
            var fileName;
            // use request.params.filename if defined
            if (request.params.filename == undefined) {
                fileName = fobj.originalFilename;
            } else {
                fileName = request.params.filename;
            }
            var file = __dirname + '/public/' + ftype + 's/' + fileName;
            console.log('Uploaded: ' + fobj.path + ' -> ' + file + ' : ' + fobj.type);
            fs.rename(fobj.path, file, function(err) {
                if (err) {
                    console.log('ERROR: could not rename to ' + file);
                } else {
                    baseDb.putMediaFile(db, ftype, fileName);     // update db
                }
            });
        }
        baseDb.sendJsonStatus(response, 200, 'Uploaded ' + count + ' files', "OK");
    }
}
****/

function handleDepthSensorDataReading(request, response) {
	var sensorid = request.params.sensorid;
	//left: 0, right: 1, front: 2, rear: 3
	var distance = request.params.distance;
	var timestamp = request.params.distance;
	var notificationData = {"sensorid" : sensorid,
				"distance" : distance,
				"timestamp" : timestamp};
	if(sensorid > 3) {
		console.log('Request: ' + JSON.stringify(request.body, undefined, 4));
	        baseDb.sendJsonStatus(response, 400, 'sensorid' + sensorid + 'not supported', "ERROR");
	}
	if(notification) {
		notification.send(JSON.stringify(notificationData));
		console.log("sent sensor data to the phone.");
		baseDb.sendJsonStatus(response, 200, 'sent success', "OK");
	}
	else {
		console.log("Could not send sensor data to the phone");
		baseDb.sendJsonStatus(response, 200, 'sent failure', "ERROR");
	}
}


function handleDoorLogFile(request, response, callback) {
    // /doorlog
    var count = Object.keys(request.files).length;
    if (count < 1) {
        console.log('Request: ' + JSON.stringify(request.body, undefined, 4));
        baseDb.sendJsonStatus(response, 400, 'No file specified to upload', "ERROR");
    } else if (count > 1) {
        baseDb.sendJsonStatus(response, 400, 'Only one file is expected', "ERROR");
    } else {
        console.log('Uploading ' + count + ' file\n'
                    + JSON.stringify(request.files.file, undefined, 4));
        var fobj = request.files.file;
        var ftype = 'file';
        var fileName;
        if (request.params.filename === undefined) {
            fileName = fobj.originalFilename;
        } else {
            fileName = request.params.filename;
        }
        var file = __dirname + '/public/' + ftype + 's/' + fileName;
        console.log('Uploaded: ' + fobj.path + ' -> ' + file + ' : ' + fobj.type);
        console.log('Renaming file to' + file);
        fs.rename(fobj.path, file, function(err) {
            if (err) {
                console.log('ERROR: could not rename to ' + file);
                baseDb.sendJsonStatus(response, 400, 'Could not upload files', "ERROR");
            } else {
                console.log('Successfully saved file');
                baseDb.putMediaFile(db, "file", fileName, null);
                baseDb.sendJsonStatus(response, 200, 'Uploaded ' + count + ' files', "OK");
                var notificationData = {deviceType:"doorlog"};
                if(notification) {
                    notification.send(JSON.stringify(notificationData));
                    console.log("sent notification for logdump available to phone");
                } else {
                    console.log("Could not send Notification for logdump");
                }
            }
            });
    }
}


function handleGetCameraLog(request,response) {
    exec('camera_status', function(err, stdout, stderr) {
        var json = { "info": "camera status" };
        if (err === null) {
            json.status = "OK";
            json.message = stdout;
        } else {
            console.log('Cannot get camera status: ' + stderr);
            json.status = "Error";
            json.message = "Unable to query camera status";
        }
        response.send(json);
    });
}

function handleGetDoorLog(request,response) {
    var file = __dirname + '/public/files/logs.tar.gz';
    response.sendfile(file);
}

function handleStatus(request, response) {
    // /status/:detail?
    var detail = 'summary';
    if (request.params.detail) {
        detail = request.params.detail;
    }
    var stats = {
        "statistics": {
            "detail": detail
        }
    };
    console.log('request statistics from controlservice');
    control.send(stats, function(jdata) {
        console.log('Got statistics from controlservice');
        if (jdata === undefined) {
            jdata = {
	        "status": {
                    "code": 404,
                    "message": 'No data returned from controlservice',
                    "state":   'error'
	        }
            };
        }
        response.send(jdata);
    });
}

/**
 * Call a device handler method.  Not all methods use param1, param2 or return a value.
 */
function callHandler(json, method, param1, param2, callback) {
    console.log('In callHandler method=' + method + ', json=' + Object.keys(json.device));
    //console.log('In callHandler method=' + method);
    //var devtype = baseDb.getDeviceType(json.device.type);
    baseDb.getDeviceType(json.device.type, function(devType) {
        console.log('Call ' + devType.handlerName + '.' + method +
                    ' for device type ' + devType.name);
        var caps = devType.handler.caps.capabilities;
        if (caps[method]) {
            //console.log(' -ret=' + ret);
            if (callback != undefined) {
		caps[method](json, param1, param2, callback);
            } else {
	        return caps[method](json, param1, param2);
	    }
        } else {
	    var msg = 'Method ' + method + ' NOT found in handler';
	    console.log(msg);
            if (callback != undefined) {
		var statmsg = baseDb.sendJsonStatus(null, 400, msg, "error");
                callback(statmsg);
            } else {
	        return msg;
	    }
        }
    });
}

/**
 * Forward REST request to another module's node.js if device is managed elsewhere.
 * If it is a local device, then 'callback' is called.
 * The 'httpmethod' parameter is optional and defaults to GET
 */
function forwardToModule(request, response, uuid, callback, httpmethod) {
    baseDb.readDevice(db, uuid, function(json) {
        if (json.device.module && json.device.module !== myModule) {
	    // device is managed by another module -- forward request
            console.log('route to module=' + json.device.module + ': ' + request.originalUrl);
            var omod = baseDb.getModule(json.device.module);
            if (omod && omod.moduleUrl) {
                var urlFwd = omod.moduleUrl + request.originalUrl;
                if (httpmethod === undefined || httpmethod === 'GET') {
                    reqmod.get(urlFwd, function(err, resp, body) {
                        if (err) console.log('fwd err: ' + err);
                        console.log('fwdGet: ' + body);
                        //console.log('fwdresp: ' + JSON.stringify(resp));
                        response.send(body);
                    });
                } else {
                    reqmod.put(urlFwd, function(err, resp, body) {
                        if (err) console.log('fwd err: ' + err);
                        console.log('fwdPut: ' + body);
                        response.send(body);
                    });
                }
            } else {
                var errmsg = 'No address found for module ' + json.device.module;
                baseDb.sendJsonStatus(response, 400, errmsg, "error");
            }
        } else {
	    // device is local to this module
            callback(json);
        }
    });
}

function handleControlDebugGet(request, response) {
    // /control/:handler/:method/:params
}

function handleControlDebugPut(request, response) {
    // /control/:handler/:method/:params
}

/* Web Interface */
app.get('/',                    handleIndex);
app.get('/index.html',          handleIndex);
app.get('/devices.html',        handleDevicesPage);
app.get('/camera.html',         handleCameraPage);
app.get('/stream.html',         handleCameraStream);
app.get('/m/lights.html',       handleLightsPage);
app.get('/m/pictures.html',     handlePicturesPage);
app.get('/m/sounds.html',       handleSoundsPageM);
app.get('/m/turnouts.html',     handleTurnoutsPage);
app.get('/media/images.html',        handleImagesPage);
app.get('/media/videos.html',        handleVideosPage);
app.get('/media/sounds.html',        handleSoundsPage);

/* REST API */
app.get('/users', user.list);
app.put('/user/spnsuuid/:uuid',      handlePutSpnsUser);
app.put('/user/notification/:notificationtype/:notificationid/:portnumber?', handlePutNotificationType);
app.put('/user/notification/depthsensor/:sensorid/:distance/:timestamp?', handleDepthSensorDataReading);
app.get('/device/:devuuid/key/:key', handleDeviceGetKey);
app.put('/device/:devuuid/key/:key/:value',
                                     handleDevicePutKey);
app.get('/device/*/setting',         handleDeviceGetSetting);
app.put('/device/*/setting/*',       handleDevicePutSetting);
app.put('/device/*/move/*',          handleDevicePutMove);
app.put('/device/*/moveto/*',        handleDevicePutMoveTo);
app.put('/device/:devuuid/program/:func/:program',
                                     handleDeviceProgramLeds);
app.get('/device/*',                 handleDevice);
app.get('/devices/type/:devtype',    handleDevicesType);
app.get('/devices',                  handleDevices);

app.get('/media/sounds',             handleSounds);
app.get('/media/sound/:sidfile',     handleSound);
app.get('/player/sound/:sidfile',    handleSoundPlayer);
app.get('/media/videos',             handleVideos);
app.get('/media/video/:vidfile',     handleVideo);
app.put('/media/video/:vidfile',     handlePutVideo);
app.delete('/media/video/:vidfile',  handleDeleteVideo);
app.get('/media/images',             handleImages);
app.get('/media/image/:imgfile',     handleImage);
app.put('/media/image/:imgfile',     handlePutImage);
app.delete('/media/image/:imgfile',  handleDeleteImage);
app.get('/media/files',              handleFiles);
app.get('/media/files/:filename',    handleFile);
app.put('/media/files/:filename',    handlePutFile);
app.delete('/media/files/:filename', handleDeleteFile);
app.post('/media/files/:filename?',  handlePostFiles);

app.put('/detect/run/:prognum',      handleRunDetect);
app.post('/detect/uploadrun/:filename?', handleRunUploadDetect);
app.put('/detect/schedule/:interval', handleScheduleDetect);

app.post('/doorlogs',handleDoorLogFile);
app.get('/getdoorlog/logs.tar.gz',handleGetDoorLog);
app.get('/cameralog', handleGetCameraLog);

app.get('/status/:detail?', handleStatus);

/* Streaming API */
app.put('/camera/:devuuid/control/:value', handleCameraControl);
app.post('/camera/:devuuid/videoclip', handleCameraVideoClip);
app.post('/camera/:devuuid/snapshot', handleCameraSnapshot);
app.get('/camera/:devuuid/getpicture', handleCameraGetPicture);
app.get('/camera/:devuuid/stream/:vidfmt', handleCameraGetStream);
app.get('/camera/:devuuid/showstream/:vidfmt', handleCameraShowStream);
// app.get('/camera/:devuuid/feeds', handleCameraFeeds);

/* TEST REST used for testing -- can be removed */
if ('development' == app.get('env')) {
    app.get('/control/:handler/:method/:params', handleControlDebugGet);
    app.put('/control/:handler/:method/:params', handleControlDebugPut);
}

http.createServer(app).listen(app.get('port'), function(){
  console.log('Node.js Express server listening on port ' + app.get('port'));
});
