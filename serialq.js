//
//   Copyright 2020 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: rest-web-front
//   Author:  Andy Warner
//
// serialq.js  chain functions together for sequential processing
//

exports.SerialQueue = function()
{
    var sq =
    {
        funcs : [],
        args : [],
        allRequestsDone : true,
        next : function()
        {
            var Q = this;
            var f = Q.funcs.shift();
            var a = Q.args.shift();
            var next_func = function() {Q.next();};
            if (!a)
                a = [];
            a.push(next_func);
            if (f)
                f.apply(null,a);
            else
                this.allRequestsDone = true;
        },
        add : function()
        {
            if (arguments.length < 1)
                return 'Error';
            this.funcs.push(arguments[0]);
            args = Array.prototype.splice.call(arguments, 1);
            this.args.push(args);
            if (this.allRequestsDone === true) {
                this.allRequestsDone = false;
                this.next();
            }
        },
    };
    return sq;
};


